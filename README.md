Cluster express app with capability to:

- Assign to a process.id ( resulting from cluster.fork() ) a custom "Master Process" that will only run in the worker designated as "MASTER: true"

- If the "Master worker" goes down, it will assign MASTER: true to the next PID from cluster.fork()

The track is done by sending events msgs from cluster.isMaster to the worker assigned.

The "master process" is a simple setInterval in app.js

Execute: > nodejs ./cluster.js