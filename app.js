var util = require('util')
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

//CLUSTER
console.log('WORKER ACTIVE: ' + cluster.worker.id);
//CLUSTER MASTER WORKER
cluster.worker.on('message', function(msg) {
    console.log(msg);
    if (msg.hasOwnProperty('msgFromMaster')) {
        if (msg.msgFromMaster.hasOwnProperty('master')) {
            if (msg.msgFromMaster.master) {
                startMaster();
            }
        }
    }
});

function startMaster() {
    setInterval(function() {
        console.log('interval execute in WORKER : ' + cluster.worker.id);
    }, 2000)

    setTimeout(function() {
        throw 'This is an throw ERROR form WORKER: ' + cluster.worker.id;
    }, 7000)
}

global.foo = new Date();
module.exports = app;