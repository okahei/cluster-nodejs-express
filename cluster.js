// Include the cluster module
global.cluster = require('cluster');
var masterWorker = {
    pid: null
}
var myMaster = 1;
var util = require('util')

// Code to run if we're in the master process
if (cluster.isMaster) {

    // Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    Object.keys(cluster.workers).forEach(function(id) {
        console.log('New WORKER PID: ' + cluster.workers[id].process.pid + ' with ID: ' + cluster.workers[id].id);
        if (cluster.workers[id].id === myMaster) {
            masterWorker.pid = cluster.workers[id].process.pid;
            cluster.workers[id].send({
                msgFromMaster: {
                    master: true
                }
            });
        }
    });

    // Listen for dying workers
    cluster.on('exit', function(worker) {
        if (worker.process.pid === masterWorker.pid) { // masterWorker ha Muerto
            var newMasterWorker = cluster.fork();
            masterWorker.pid = newMasterWorker.process.pid;
            console.log('PID new masterWorker : ' + util.inspect(newMasterWorker.process.pid));
            newMasterWorker.send({
                msgFromMaster: {
                    master: true
                }
            });
        } else {
            cluster.fork();
        }
        // Replace the dead worker, we're not sentimental
        console.log('Worker %d died :(', worker.id);
    });

    cluster.on('fork', function(worker) {});

    // Code to run if we're in a worker process
} else {
    require("./bin/www");
}